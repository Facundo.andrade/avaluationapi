const express = require("express"); // es el nostre servidor web
const jwt = require("jsonwebtoken");
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const bodyParser = require('body-parser'); // per a poder rebre jsons en el body de la resposta
const app = express();
const baseUrl = '/miapi';
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use(bodyParser.urlencoded({ extended: false })); //per a poder rebre json en el reuest
app.use(bodyParser.json());

//La configuració de la meva bbdd

//El pool es un congunt de conexions
//const Pool = require('pg').Pool
//const pool = new Pool(ddbbConfig);

var mysql = require('mysql');
var connection = mysql.createConnection({
    user: '234185',
    host: 'mysql-avaluation.alwaysdata.net',
    database: 'avaluation_bbdd',
    password: 'ProjecteAvaluation',
    port: 3306
});



//Exemple endPoint
//Quan accedint a http://localhost:3000/miapi/test   ens saludará
const getAlumnos = (request, response) => {

    connection.connect();

    var consulta = "SELECT * FROM Alumno"

    connection.query(consulta, function (err, rows, fields) {
        if (err) throw err;
        console.log(rows);
        response.status(200).json(rows)
    });

    connection.end();
}

app.get(baseUrl + '/getAlumnos', getAlumnos);


const getLogin = (request, response) => {

    connection.connect();

    console.log("este"+request.body);
    const {dni,pass}=request.body;

    var consulta = `SELECT pass_alumno FROM Alumno WHERE dni_alumno='${dni}'`;
    let user_pass;

    connection.query(consulta, function (err, rows, fields) {

        if (err) throw error;
        console.log(rows);

        user_pass=rows[0].pass_alumno;

        if (user_pass == pass){

            console.log("TODO OK")

            const user = {
                nombre: dni
            }

            jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
                response.status(200).json({
                    message: "Nice!",
                    token
                })
            })
            
        }else{

            response.status(401).json({
                message: "error de autentificacion"
            })

        }
        //response.status(200).json(rows)  
    });

    connection.end();
}

app.post(baseUrl + '/Login', getLogin);

/*app.post(baseUrl+'/loginuser', (req, res) => {
    const user = {
        id:1,
        nombre: "Facu",
        email: "facu@email.com"
    }

    jwt.sign({user: user}, 'secretKey',{expiresIn: '32s'}, (err, token) => {
        res.json({
            token
        })
    })


});*/


app.post(baseUrl+'/posts',verifyToken, (req, res) => {

    jwt.verify(req.token, 'secretKey', (err, authData)=> {
        if(err){
            res.sendStatus(403);
        }else{
            res.json({
                mensaje: "Post fue creado",
                authData
            });
        }
    })
});

//Authorization: Bearer <token>
function verifyToken(req,res,next){
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader !== 'undefined'){
        const bearerToken = bearerHeader.split(" ")[1];
        req.token = bearerToken;
        next();

    }else{

        res.sendStatus(403);
    }
}

//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
    console.log("El servidor está inicialitzat en el puerto " + PORT);
});



